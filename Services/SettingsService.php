<?php

namespace CYINT\ComponentsPHP\Bundles\SettingsBundle\Services;

use CYINT\ComponentsPHP\Bundles\SettingsBundle\Entity\Setting;

class SettingsService
{
    private $Doctrine;
    private $Repository;

    public function __construct($Doctrine)
    {
        $this->Doctrine = $Doctrine;
        $this->Repository = $Doctrine->getRepository('CYINTSettingsBundle:Setting');
    }

    public function getSetting($key, $default = null)
    {
        $result = $this->Repository->findBy(array('settingKey'=>$key));
        if(!empty($result))
        {
            $Setting = array_pop($result);
            $value = $Setting->getValue();
            return empty($value) ? $default : $value;
        }

        return $default;
    }

    public function setSetting($key, $value)
    {
        $result = $this->Repository->findOneBy(array('settingKey'=>$key));
        if(empty($result))
        {
            $result = new Setting();      
        }

        $result->setSettingKey($key);
        $result->setValue($value);

        $this->Doctrine->getManager()->persist($result);
        $this->Doctrine->getManager()->flush();

        return $result;       
    }
}

?>
