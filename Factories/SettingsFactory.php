<?php

namespace CYINT\ComponentsPHP\Bundles\SettingsBundle\Factories;

use CYINT\ComponentsPHP\Bundles\SettingsBundle\Entity\Setting;

class SettingsFactory
{
    public function createSetting($key, $value)
    {
        $Setting = new Setting($key, $value);
        return $Setting;
    }

    public function updateSetting(Setting $Setting, $key, $value)
    {
        $Setting->setValue($value);
        $Setting->setSettingKey($key);
        return $Setting;
    }
}
